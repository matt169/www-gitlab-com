<!-- PLEASE READ: See https://about.gitlab.com/handbook/marketing/blog/ for details on the blog process -->

<!-- All blog posts should have a corresponding issue. Create an issue now if you haven't already, and add the link in place of the placeholder link below -->
Closes https://gitlab.com/gitlab-com/www-gitlab-com/issues/XXXX

### Checklist

- [ ] Link to issue added, and set to close when this MR is merged
- [ ] Due date and milestone (if applicable) added for the desired publish date
- [ ] If [time sensitive](https://about.gitlab.com/handbook/marketing/blog/#time-sensitive-posts-official-announcements-company-updates-breaking-changes-and-news)
  - [ ] Added ~"priority" label
  - [ ] Mentioned `@rebecca` to give her a heads up ASAP
- [ ] Blog post file formatted correctly, including any accompanying images
- [ ] Review app checked for any formatting issues
- [ ] Reviewed by fellow team member
- [ ] Reviewed by [content team](https://about.gitlab.com/handbook/marketing/blog/#editorial-reviews)

/label ~"blog post"
