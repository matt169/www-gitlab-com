require 'date'

module MaturityHelpers
  def maturity(category, milestone = Date.today)
    if milestone.today?
      category.maturity || "planned"
    elsif category.lovable && category.lovable <= milestone
      "lovable"
    elsif category.complete && category.complete <= milestone
      "complete"
    elsif category.viable && category.viable <= milestone
      "viable"
    elsif category.available && category.available <= milestone
      "minimal"
    else
      "planned"
    end
  end

  # Build the set of X axis points when maturity states change
  def get_category_maturity_dates(categories, start_date)
    dates = []

    dates.push(start_date.strftime('%Y-%m-%d'))
    dates.push(Date.today.strftime('%Y-%m-%d'))
    dates |= categories.select { |category_key, category| category.available && category.available >= start_date }.collect { |category_key, category| category.available.strftime '%Y-%m-%d' }
    dates |= categories.select { |category_key, category| category.minimal && category.minimal >= start_date }.collect { |category_key, category| category.minimal.strftime '%Y-%m-%d' }
    dates |= categories.select { |category_key, category| category.viable && category.viable >= start_date }.collect { |category_key, category| category.viable.strftime '%Y-%m-%d' }
    dates |= categories.select { |category_key, category| category.complete && category.complete >= start_date }.collect { |category_key, category| category.complete.strftime '%Y-%m-%d' }
    dates |= categories.select { |category_key, category| category.lovable && category.lovable >= start_date }.collect { |category_key, category| category.lovable.strftime '%Y-%m-%d' }

    dates.uniq.sort
  end

  def maturity_chart(stage: nil, iso_start_date: "2019-01-01", embed: false)
    start_date = Time.parse(iso_start_date)

    # Get the set of categories, filtered if desired to a certain stage
    categories =  if stage
                    data.categories.select { |category_key, category| category.stage == stage && category.marketing }
                  else
                    data.categories.select { |category_key, category| category.marketing == true }
                  end

    dates = get_category_maturity_dates(categories, start_date)

    planned = []
    minimal = []
    viable = []
    complete = []
    lovable = []

    dates.each_with_index do |date, index|
      planned[index] = 0
      minimal[index] = 0
      viable[index] = 0
      complete[index] = 0
      lovable[index] = 0

      categories.each do |category_key, category|
        case maturity(category, Date.parse(date))
        when "planned"
          planned[index] += 1
        when "minimal"
          minimal[index] += 1
        when "viable"
          viable[index] += 1
        when "complete"
          complete[index] += 1
        when "lovable"
          lovable[index] += 1
        end
      end
    end

    partial('includes/maturity-chart.html.erb', locals: { stage: stage, dates: dates, planned: planned, minimal: minimal, viable: viable, complete: complete, lovable: lovable, embed: embed })
  end
end
