---
layout: markdown_page
title: "Sarah O'Donnell’s README"
---
### What is this?
This is my [Manager README](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe), a document that helps you learn a little more about me, how I think, and how I work.  The intended audience is anyone who reports into me, though anyone is free to read it - or even provide feedback on it.

### About me

My name is Sarah O’Donnell and I’m GitLab’s UX Research Manager. 

* [GitLab Handle](https://gitlab.com/sarahod)  
* [Team Page](https://about.gitlab.com/company/team/#sarahod)  

I joined GitLab in November 2016 as GitLab’s first and only UX Researcher.  I built GitLab’s research practice from the ground up.

### My role

I see my role as follows:

* To provide context. Are you missing something? Let me know and I’ll fill you in or go find out.
* Be a sounding board. I won’t be working on your research studies day to day but I will be close enough to have informed thoughts.
* To advocate for you and the UX Research team with the rest of the company.
* To ensure the UX Research team is aligned and pushing in the same direction.
* Most importantly, I am here to make sure you are both successful and happy. I want you to improve your skills, grow your career and enjoy your work.

### Working with me

* Generally, I prefer action over lengthy discussions and debate. I have faith in your ability and believe that testing your idea is the best way to start learning and making progress.

* As we grow as a team, I strongly believe in changing existing processes to accommodate our needs. I don’t want you do any process that is not beneficial to you and which prevents you from doing your best work. Nothing is set in stone and we don’t do anything “because we’ve always done it that way”.

* I’m naturally a quiet person but I’m very observant and I listen intently (hence one of the reasons why I became a UX Researcher!). Please don’t mistake my silence for disinterest. I’m likely thinking over the topic at hand before sharing my viewpoint. I don’t believe you need to be the loudest voice in the room to have the most impact. 

* I believe candid and constructive feedback is critical to success. Lets be honest with each other. If there's a way in which I can improve, please let me know.

### 1:1s

* 1:1s are a dedicated time for you to talk about anything and everything you want. It needn’t be just a status update. We’ll meet for 30 minutes each week. I encourage you to write down talking points throughout the week; it can be hard to think of things in the moment. 1:1s are flexible, they can shift and change to fit both of our schedules.

* Stuck on something? Something bothering you? Please tell me right away. We don’t need to wait for our next scheduled 1:1 to chat. Feel free to put something in my calendar, there's no need to ask permission first. If my calendar is full, send me a Slack message and I’ll likely be able to move something around.

### Outside of work

* I live in Manchester, UK (GMT/BST) with my SO, two dogs and bearded dragon. I enjoy reading (mostly non-fiction), music (all genres), good food and spending time outdoors. 